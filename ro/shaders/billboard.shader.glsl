// more shaders at http://renderingwonders.wordpress.com/category/glsl/
// That's pretty cool. I've been drawing billboards by taking﻿ the modelview matrix, and loading the identity just on the 3x3 matrix (the whole thing is 4x4).
// http://ofps.oreilly.com/titles/9780596804824/ch2d.html#PointSprites

#vertex

// (RO) matrices
uniform mat4 proj;
uniform mat4 view;

// (RW) input parameters for our vertex shader
attribute vec4 position;
attribute vec2 texCoord0;

// (RW->frag) input parameters for our pixel shader
// also the output parameters for our vertex shader
varying vec4 v_texcoord;

void main() 
{
  // Just add the world position of the transform to input position
  // and then multiple by the projection.
  //gl_Position = proj * (position + vec4(view[3].xyz, 0));
  gl_Position = ( proj * view ) * vec4( position.xyz, 1 );
  v_texcoord = gl_TexCoord[0]; //texCoord0;
}

#fragment

uniform sampler2D texture;
varying vec4 v_texcoord;

void main()
{
  gl_FragColor = texture2D(texture, v_texcoord.st); //.st?
}

