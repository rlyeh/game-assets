#vertex
#version 120
#define lowp
#define mediump
#define highp

attribute vec4 coord;
uniform mat4 mvp;
varying vec4 texcoord;

void main(void)
{
	// Just pass the original vertex coordinates to the fragment shader as texture coordinates
	texcoord = coord;

	// Apply the model-view-projection matrix to the xyz components of the vertex coordinates
	vec4 vertexPosClip = mvp * vec4(coord.xyz * vec3(1,1,1), 1);
	gl_Position = vertexPosClip;

	// [ref] http://www.gamedev.net/blog/73/entry-2006307-tip-of-the-day-logarithmic-zbuffer-artifacts-fix/
	// [ref] http://outerra.blogspot.com.es/2009/08/logarithmic-z-buffer.html
	gl_TexCoord[6] = vertexPosClip;
}

#fragment
#version 120
#define lowp
#define mediump
#define highp

varying vec4 texcoord;
uniform sampler2D texture_;

//const vec4 fogcolor = vec4(1.0, 0.8, 0.6, 1.0); // sand
//const float fogdensity = .0003;

//const vec4 fogcolor = vec4(0, 0, 0, 1.0); // night
//const float fogdensity = .0003;

const vec4 fogcolor = vec4(0.6, 0.8, 1.0, 1.0); // smog
const float fogdensity = .0001;

void main(void) {
	vec2 coord2d;
	float intensity;

	// If the texture index is negative, it is a top or bottom face, otherwise a side face
	// Side faces are less bright than top faces, simulating a sun at noon
	if(texcoord.w < 0.0) {
		coord2d = vec2((fract(texcoord.x) + texcoord.w) / 16.0, texcoord.z);
		intensity = 1.0;
	} else {
		coord2d = vec2((fract(texcoord.x + texcoord.z) + texcoord.w) / 16.0, -texcoord.y);
		intensity = 0.45;
	}

	vec4 color = texture2D(texture_, coord2d);

	// Very cheap "transparency": don't draw pixels with a low alpha value
	if(color.a < 0.4)
		discard;

	// Attenuate sides of blocks
	color.xyz *= intensity;

	// Calculate strength of fog
	float z = gl_FragCoord.z / gl_FragCoord.w;
	float fog = clamp(exp(-fogdensity * z * z), 0.2, 1.0);

	// Final color is a mix of the actual color and the fog color
	gl_FragColor = mix(fogcolor, color, fog);

	// [ref] http://www.gamedev.net/blog/73/entry-2006307-tip-of-the-day-logarithmic-zbuffer-artifacts-fix/
	// [ref] http://outerra.blogspot.com.es/2009/08/logarithmic-z-buffer.html
	const float C = 1.0;
	const float far = 1000000000.0;
	const float offset = 1.0;
	// gl_FragDepth = (log(C * gl_TexCoord[6].z + offset) / log(C * far + offset));
}



/*

// from http://codeflow.org/entries/2010/dec/09/minecraft-like-rendering-experiments-in-opengl-4/

	struct SHC{
	    vec3 L00, L1m1, L10, L11, L2m2, L2m1, L20, L21, L22;
	};

	SHC groove = SHC(
	    vec3( 0.3783264,  0.4260425,  0.4504587),
	    vec3( 0.2887813,  0.3586803,  0.4147053),
	    vec3( 0.0379030,  0.0295216,  0.0098567),
	    vec3(-0.1033028, -0.1031690, -0.0884924),
	    vec3(-0.0621750, -0.0554432, -0.0396779),
	    vec3( 0.0077820, -0.0148312, -0.0471301),
	    vec3(-0.0935561, -0.1254260, -0.1525629),
	    vec3(-0.0572703, -0.0502192, -0.0363410),
	    vec3( 0.0203348, -0.0044201, -0.0452180)
	);

	SHC beach = SHC(
	    vec3( 0.6841148,  0.6929004,  0.7069543),
	    vec3( 0.3173355,  0.3694407,  0.4406839),
	    vec3(-0.1747193, -0.1737154, -0.1657420),
	    vec3(-0.4496467, -0.4155184, -0.3416573),
	    vec3(-0.1690202, -0.1703022, -0.1525870),
	    vec3(-0.0837808, -0.0940454, -0.1027518),
	    vec3(-0.0319670, -0.0214051, -0.0147691),
	    vec3( 0.1641816,  0.1377558,  0.1010403),
	    vec3( 0.3697189,  0.3097930,  0.2029923)
	);

	SHC tomb = SHC(
	    vec3( 1.0351604,  0.7603549,  0.7074635),
	    vec3( 0.4442150,  0.3430402,  0.3403777),
	    vec3(-0.2247797, -0.1828517, -0.1705181),
	    vec3( 0.7110400,  0.5423169,  0.5587956),
	    vec3( 0.6430452,  0.4971454,  0.5156357),
	    vec3(-0.1150112, -0.0936603, -0.0839287),
	    vec3(-0.3742487, -0.2755962, -0.2875017),
	    vec3(-0.1694954, -0.1343096, -0.1335315),
	    vec3( 0.5515260,  0.4222179,  0.4162488)
	);

	vec3 sh_light(vec3 normal, SHC l){
	    float x = normal.x;
	    float y = normal.y;
	    float z = normal.z;

	    const float C1 = 0.429043;
	    const float C2 = 0.511664;
	    const float C3 = 0.743125;
	    const float C4 = 0.886227;
	    const float C5 = 0.247708;

	    return (
	        C1 * l.L22 * (x * x - y * y) +
	        C3 * l.L20 * z * z +
	        C4 * l.L00 -
	        C5 * l.L20 +
	        2.0 * C1 * l.L2m2 * x * y +
	        2.0 * C1 * l.L21  * x * z +
	        2.0 * C1 * l.L2m1 * y * z +
	        2.0 * C2 * l.L11  * x +
	        2.0 * C2 * l.L1m1 * y +
	        2.0 * C2 * l.L10  * z
	    );
	}

vec3 get_eye_normal(vec2 viewport, mat4 inverse_projection){
vec4 device_normal = vec4(((gl_FragCoord.xy/viewport)-0.5)*2.0, 0.0, 1.0);
return normalize((inverse_projection * device_normal).xyz);
}

vec3 gamma(vec3 color){
return pow(color, vec3(1.0/2.0));
}

vec3 fog(vec3 color, vec3 fcolor, float depth, float density){
const float e = 2.71828182845904523536028747135266249;
float f = pow(e, -pow(depth*density, 2));
return mix(fcolor, color, f);
}


	#version 400

vertex:
    in float occlusion;
    in vec3 normal, texcoord, s, t, light;
    in vec4 position;
    uniform mat4 mvp, modelview;

    out Data{
        vec3 texcoord, light;
        float depth, occlusion;
        mat3 matfn;
    } data;

    void main(void){
        data.occlusion = occlusion;
        data.texcoord = texcoord;
        data.depth = length((modelview * position).xyz);
        data.matfn = transpose(mat3(s, normal, t));
        data.light = light;
        gl_Position = mvp * position;
    }

fragment:
    import: spherical_harmonics
    import: util

    uniform vec2 viewport;
    uniform mat3 normalmatrix;
    uniform mat4 inverse_projection;
    uniform sampler2DArray material, normalmap, specularmap;

    in Data{
        vec3 texcoord, light;
        float depth, occlusion;
        mat3 matfn;
    } data;

    out vec3 fragment;

    void main(){
        vec3 eye_normal = get_eye_normal(viewport, inverse_projection);
        vec3 material_color = texture(material, data.texcoord).rgb;
        vec3 map_normal = normalize(texture(normalmap, data.texcoord).rgb);
        vec3 frag_normal = normalize(map_normal * data.matfn);
        vec3 eye_frag_normal = normalize(normalmatrix * frag_normal);

        vec3 outside = sh_light(frag_normal, beach);
        vec3 inside = sh_light(frag_normal, groove)*0.004;
        vec3 ambient = mix(outside, inside, data.occlusion);

        float frag_specular = texture(specularmap, data.texcoord).r;
        vec3 torch_color = vec3(1.0, 0.83, 0.42);
        float intensity = 2.0/pow(data.depth, 2);
        float lambert = abs(min(0, dot(eye_frag_normal, eye_normal)));
        float specular = pow(lambert, 1+frag_specular*8);
        vec3 torch = specular * intensity * torch_color;
        float highlight = pow(specular, 4) * intensity * frag_specular;

        vec3 color = (
            material_color * ambient +
            material_color * torch +
            highlight * torch_color +
            material_color * data.light
        );

        vec3 at_observer = fog(color, vec3(0.8), data.depth, 0.005);
        fragment = gamma(at_observer);
    }

*/
