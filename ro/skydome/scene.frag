
uniform sampler2D       diffuse;
uniform sampler2DShadow shadow0;
uniform sampler2DShadow shadow1;
uniform sampler2DShadow shadow2;
uniform sampler2DShadow shadow3;
uniform sampler2D       lightmask;

varying vec3 position;
varying vec4 vertex;
varying vec3 normal;

void main()
{
    // Look up the diffuse color and shadow states for each light source.

    vec4  Kd = texture2D   (diffuse, gl_TexCoord[0].xy);
    float s0 = shadow2DProj(shadow0, gl_TexCoord[1]).r;
    float s1 = shadow2DProj(shadow1, gl_TexCoord[2]).r;
    float s2 = shadow2DProj(shadow2, gl_TexCoord[3]).r;
    float s3 = shadow2DProj(shadow3, gl_TexCoord[4]).r;

    // Clamp the range of the spot light sources.

    float z2 = gl_TexCoord[3].z / gl_TexCoord[3].w;
    float z3 = gl_TexCoord[4].z / gl_TexCoord[4].w;

    float c2 = step(0.0, z2) * step(z2, 1.0);
    float c3 = step(0.0, z3) * step(z3, 1.0);

    // Compute the attenuation of the spot light sources.

    float a2 = c2 / length(gl_LightSource[2].position.xyz - vertex.xyz);
    float a3 = c3 / length(gl_LightSource[3].position.xyz - vertex.xyz);

    // Look up the light masks for the spot light sources.

    vec3  m2 = texture2DProj(lightmask, gl_TexCoord[3]).rgb;
    vec3  m3 = texture2DProj(lightmask, gl_TexCoord[4]).rgb;

    // Compute the lighting vectors.

    vec3 N  = normalize(normal);
    vec3 L0 = normalize(gl_LightSource[0].position.xyz);
    vec3 L1 = normalize(gl_LightSource[1].position.xyz);
    vec3 L2 = normalize(gl_LightSource[2].position.xyz - vertex.xyz);
    vec3 L3 = normalize(gl_LightSource[3].position.xyz - vertex.xyz);

    // Compute the illumination coefficient for each light source.

    vec3  d0 = vec3(max(dot(N, L0), 0.0)) * s0;
    vec3  d1 = vec3(max(dot(N, L1), 0.0)) * s1;
    vec3  d2 = vec3(max(dot(N, L2), 0.0)) * s2 * a2 * m2
             + gl_FrontMaterial.emission.r;
    vec3  d3 = vec3(max(dot(N, L3), 0.0)) * s3 * a3 * m3
             + gl_FrontMaterial.emission.g;

    // Compute the scene foreground/background blending coefficient.

    float fade = 1.0 - smoothstep(48.0, 64.0, length(position));

    // Compute the final pixel color from the diffuse and ambient lighting.

    float alpha = Kd.a * fade;
    vec3 color = Kd.rgb * (gl_LightSource[0].diffuse.rgb * d0 +
                                  gl_LightSource[1].diffuse.rgb * d1 +
                                  gl_LightSource[2].diffuse.rgb * d2 +
                                  gl_LightSource[3].diffuse.rgb * d3 +
                                  gl_LightModel.ambient.rgb);

    gl_FragColor = vec4( color, alpha );
}
