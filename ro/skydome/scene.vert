
uniform mat4 mvp;

varying vec3 position;
varying vec4 vertex;
varying vec3 normal;

void main()
{
    position = gl_Vertex.xyz;

    vertex = gl_ModelViewMatrix * gl_Vertex;
    normal = gl_NormalMatrix    * gl_Normal;

    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_TexCoord[1] = gl_TextureMatrix[1] * vertex;
    gl_TexCoord[2] = gl_TextureMatrix[2] * vertex;
    gl_TexCoord[3] = gl_TextureMatrix[3] * vertex;
    gl_TexCoord[4] = gl_TextureMatrix[4] * vertex;

    gl_Position = mvp * vec4( position, 1 );
}
