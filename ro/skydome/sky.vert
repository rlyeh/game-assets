
uniform mat4 mvp;
varying vec3 vertex;

void main()
{
    vertex      = gl_Vertex.xyz;
    gl_Position = mvp * vec4(vertex,1);
}
